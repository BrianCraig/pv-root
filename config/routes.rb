Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  defaults format: 'json' do
    resources :products
    resources :produuucts
    resources :users
    resources :sessions
    resources :sales do
      resources :sold_products
    end
  end
end
