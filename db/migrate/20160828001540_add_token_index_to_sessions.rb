class AddTokenIndexToSessions < ActiveRecord::Migration[5.0]
  def change
    add_index :sessions, :token
  end
end
