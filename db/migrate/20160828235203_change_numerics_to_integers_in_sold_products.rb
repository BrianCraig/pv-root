class ChangeNumericsToIntegersInSoldProducts < ActiveRecord::Migration[5.0]
  def up
    change_column :sold_products, :quantity, :integer
  end
  def down
    change_column :sold_products, :quantity, :numeric
  end
end
