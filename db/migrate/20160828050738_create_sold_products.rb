class CreateSoldProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :sold_products do |t|
      t.references :sale, foreign_key: true, index: true
      t.references :product, foreign_key: true
      t.numeric :price
      t.numeric :quantity

      t.timestamps
    end
  end
end
