# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

u1 = User.create(name: "brian", password:"asd123")
u2 = User.create(name: "juan", password:"asd123")
u3 = User.create(name: "marta", password:"asd123")
p1 = u1.products.create(name: "Coca cola 2LTs", price: 30)
p2 = u1.products.create(name: "Coca cola Botella", price: 18)
p3 = u1.products.create(name: "Playadito", price: 35)
p4 = u1.products.create(name: "Cruz de malta", price: 20)
s1 = u1.sales.create()
s1.sold_products.create(quantity: 1, product: p1)
s1.sold_products.create(quantity: 2, product: p2)
s1.sold_products.create(quantity: 1, product: p4)
