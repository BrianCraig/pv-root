class User < ApplicationRecord
  has_many :sessions
  has_many :products
  has_many :sales
  has_secure_password

  def as_json(options = {})
    super(options.merge({ except: [:password_digest] }))
  end

end
