class Sale < ApplicationRecord
  belongs_to :user
  has_many :sold_products

  
  def as_json(options = {})
    super(options.merge({ include: [:sold_products] }))
  end 
  
end
