class SoldProduct < ApplicationRecord
  belongs_to :sale
  belongs_to :product
  validates :quantity, presence: true
  before_create :set_price

  def set_price
    self.price = self.product.price
  end
end
