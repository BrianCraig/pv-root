class ApplicationController < ActionController::API
  def current_session
    @current_session ||= Session.find_by(token: token)
  end

  def current_user
   @current_session.user
  end

  def authorize
    render :status => :unauthorized unless current_session
  end

  private
    
    def token
      request.headers["token"]
    end

end