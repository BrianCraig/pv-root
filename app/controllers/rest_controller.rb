class RestController < ApplicationController
  class << self
    attr_accessor :record, :require, :permit
  end

  def index
    render json: models
  end

  def show
    render json: model
  end

  def update
    render json: model.update(ver_params)
  end

  def create
    render json: self.class.record.new(ver_params).save
  end

  def destroy
    render json: model.destroy
  end

  private 

    def model
      self.class.record.find(params[:id])
    end

    def models
      self.class.record.all
    end

    def ver_params
      params.require(self.class.require).permit(self.class.permit)
    end      
end