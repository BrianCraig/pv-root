class ProductsController < ApplicationController
  before_filter :authorize

  def index
    render json: products
  end

  def show
    render json: product
  end

  def update
    render json: product.update(ver_params)
  end

  def create
    prod = Product.new(ver_params)
    if prod.save then render json: prod end
  end

  def destroy
    render json: product.destroy
  end

  private 

    def product
      current_user.products.find(params[:id])
    end

    def products
      current_user.products.all
    end

    def ver_params
      params.require(:product).permit(:name, :category)
    end      
end
