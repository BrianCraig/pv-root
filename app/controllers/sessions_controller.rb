class SessionsController < ApplicationController
  before_filter :authorize, :only => [:index]

  def index
    render json: current_user.sessions.all
  end

  def create
    user = User.find_by(name: ver_params[:name]).try(:authenticate, ver_params[:password])
    if user 
      token = user.sessions.create(token: SecureRandom.hex(8))
      render json: token
    else 
      render :status => :unauthorized
    end
  end

  private
    def ver_params
      @params ||= params.permit([:name, :password])
    end
end
