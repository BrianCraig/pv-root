class UsersController < ApplicationController
  def show
    render json: user
  end

  def create
    u = User.new(ver_params)
    u.save 
    render json: u
  end

  private 

    def user
      User.find(params[:id])
    end

    def ver_params
      params.permit([:name, :password])
    end      
end
