class SalesController < ApplicationController
  before_filter :authorize

  def index
    render json: sales
  end

  def show
    render json: sale
  end

  def create
    sale = Sale.new(user: current_user)
    if sale.save then render json: sale end
  end

  private 

    def sale
      Sale.find(params[:id])
    end

    def sales
      current_user.sales.all.includes(:sold_products)
    end
end
