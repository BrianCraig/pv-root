class SoldProductsController < ApplicationController
  before_filter :authorize

  def create
    if sale 
      model = sale.sold_products.new(ver_params)
      if model.save then render json: model end
    else
      render :status => 418
    end
  end

  private 

    def sale
      @sale ||= Sale.find(params[:sale_id])
    end

    def ver_params
      params.permit(:product_id, :quantity)
    end      
end
